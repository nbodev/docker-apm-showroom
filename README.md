<h1 align="center">🐳 APM Showroom 🐳</h1>

### 📋 Introduction

- The goal of this project is to setup the required stack for monitoring, logs indexation and tracing tools.
- See [the monitoring part](./consul-prometheus-grafana/README.md) for more details.

### TODO
- Logs indexation.
- Tracing tools.