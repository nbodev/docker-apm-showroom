<h1 align="center">🐳 APM Showroom Consul, Prometheus and Grafana 🐳</h1>

### 📋 Introduction

- Install Docker.
- Run the following:

```
cd consul-prometheus-grafana
docker-compose up
```

### 💻 Consul

- Go to Consul: `http://localhost:8500/`

### 💻 Prometheus

- Go to Prometheus: `http://localhost:9092/`

### 💻 Grafana

- Go to `http://localhost:3000/`.
- Login with `admin/admin`.
- Add the Prometheus data source, use `http://apm-showroom-prometheus:9090` as url.
- Import the dashboard located in `docker-apm-showroom/consul-prometheus-grafana/grafana/dashboard/APM Showroom.json`.

### 💻 Cleanup and restart

- `docker-compose stop`: stops running containers without removing them.
- `docker-compose down`: stops containers and removes containers, networks, volumes, and images created by up.
- Run the script `clean.sh` located in the consul-prometheus-grafana root directory in order to remove the Consul and Prometheus persisted data, run `docker-compose stop` before that script.

### 📡 Links

Detailed version of this project
- https://nbodev.medium.com/application-performance-monitoring-monitor-dynamically-java-applications-with-consul-prometheus-2618fa712bfd
